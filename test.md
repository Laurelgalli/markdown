---
title: "DataByte: A Snapshot of Hospitalizations and Costs of EVALI in Utah"
author: "Brantley Scott"
date: "10/1/2021"
output: html_document
---
<style type="text/css">
  body{
  font-family: "Libre Franklin", "Open Sans", "Helvetica Neue";
  font-size: 12pt;
}
</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE)
library(tidyverse)
library(ggthemes)
library(plotly)

theme_new <-  theme_fivethirtyeight(base_size=12, base_family="Helvetica Neue") %+replace% theme(panel.grid.major.y = element_line(colour = "grey80", size = 0.25),  panel.grid.major.x = element_blank(), panel.background = element_rect(fill = "white"), plot.background = element_rect(fill = "white"), legend.background = element_rect(fill = "white")) 
```

According to [Yale Medicine](https://www.yalemedicine.org/conditions/evali), e-cigarette and vaping associated lung illness (EVALI) is defined as a significant medical condition in which a patient’s lungs become injured from ingredients contained in e-cigarettes and vaping products. Symptoms of EVALI can include, but are not limited to, shortness of breath, rapid heartbeat, and shallow breathing. The main cause of EVALI according to a [study]("https://www.yalemedicine.org/conditions/evali) published in 2019 was Vitamin E Acetate which was found to have damaged the lungs of patients that used e-cigarettes (“E-cigarette, or Vaping Product, Use Associated Lung Injury (EVALI)”). 
